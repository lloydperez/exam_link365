import axios from 'axios';
import { HTTP_TIMEOUT_LONG, HTTP_TIMEOUT_SHORT } from '../constants/httpConstants';

const http = axios.create({
  baseURL: 'http://45.77.131.145/api/',
  timeout: HTTP_TIMEOUT_LONG
});

export const localhttp = axios.create({
  baseURL: 'http://localhost:8000/api/',
  timeout: HTTP_TIMEOUT_SHORT
});

export default http;
