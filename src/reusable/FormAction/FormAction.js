import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import Button from '@material-ui/core/Button';

const FormAction = ({ 
        handleSave, 
        isDisabledAction, 
        handleClear
    }) => (

    <Fragment>
        <div className="formAction">
            {
                handleSave ? <Button
                    htmlId="saveButton"
                    onClick={handleSave}
                    text="Save"
                    variant="contained" 
                    color="primary"
                    disabled={isDisabledAction}
                >
                    Save
                </Button> : null
            }
            {
                handleClear ? <Button
                    htmlId="resetButton"
                    onClick={handleClear}
                    text="Reset Default"
                    variant="contained"
                    disabled={isDisabledAction}
                >
                    Reset
                </Button> : null
            }
        </div>
    </Fragment>
);

FormAction.propTypes = {
    handleSave: PropTypes.func,
    handleClear: PropTypes.func
};

export default FormAction;
