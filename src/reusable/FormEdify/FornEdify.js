import React, { Component, Fragment } from 'react';
import TextField from '@material-ui/core/TextField';
import Select from '@material-ui/core/Select';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';

class FormEdify extends Component {
    state = {
        data: {},
        isDoneGeneratedProps: false,
        formJSON: []
    }

    static getDerivedStateFromProps(nextProps, state) {
        const { validationResult, errorValidation } = nextProps;
        return { formJSON: nextProps.fields, data: nextProps.data, validationResult, errorValidation };
    }

    renderComponent = (item) => {
        if (item.type === undefined) {
            return null;
        }
        if (item.type ==='textInput') {
            return this.generateTextField(item);
        }
        if (item.type ==='selectInput') {
            return this.generateSelectField(item);
        } else {
            return null;
        } 
    }

    generateTextField = (item) => {
        const { data } = this.state;
        const id = item.dataIndex;
        const name = item.label;
        const variant = item.variant || 'filled';
        const value = data[item.dataIndex] || '';
        const errorMessage = this.errorMessage(item);

        return (
            <Fragment>
                <TextField
                    id={id}
                    label={name}
                    value={value}
                    onChange={item.onChange}
                    variant={variant}
                    name={item.dataIndex}
                    onBlur={item.validate}
                    error={errorMessage.length > 0 ? true : false}
                    helperText={errorMessage}
                    required={true}
                />
            </Fragment>
        )
    }

    generateSelectField = (item) => {
        const { data } = this.state;
        const id = item.dataIndex;
        const name = item.label;
        const variant = item.variant || 'filled'
        const value = data[item.dataIndex] || '';
        const options = item.options || [];
        const defaultValue = item.defaultValue || 'none';
        const error = item.error || false;
        const isDisabled = item.isDisabled || '';

        return (
            <FormControl variant={variant} disabled={isDisabled}>
                <InputLabel id={id}>{ name }</InputLabel>
                <Select
                    labelId={id}
                    id={id}
                    value={value}
                    onChange={item.onChange}
                    displayEmpty
                    inputProps={{ 'aria-label': 'Without label' }}
                    name={item.dataIndex}
                    defaultValue={defaultValue}
                    error={error}
                    onBlur={item.validate}
                    required={true}
                >
                    {
                        options.map((option, i) => {
                            return ( <MenuItem key={i} value={option.value}>{option.label}</MenuItem> )
                        })
                    }
                </Select>
            </FormControl>
        )
    }

    errorMessage = (item) => {
        const { errorValidation } = this.state;
        let errorMessage = []

        errorValidation.map(error => {
            if (error.errorField === item.dataIndex) {
                errorMessage = error.errorMessage;
            }

            return false;
        });
        
        return errorMessage;
    }

    render() {
        const arr = this.state.formJSON.map((item, i) => {
            const colItems = item.columns.map(column => {
                return (
                    <span key={i}>
                        {this.renderComponent(column)}
                    </span>
                );
            });

            return (<div className="generated-fields" key={i} >
                    {colItems}
                </div>);
        });

        return (
            <Fragment>
                { arr }
            </Fragment>
        );
    }
}

export default FormEdify;
