import React, { Component, Fragment } from 'react';
import { generateForm } from './FormFields';
import FormEdify from '../../reusable/FormEdify/FornEdify';
import FormAction from '../../reusable/FormAction/FormAction';
import http from '../../api/http';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';

class Form extends Component {
    state = {
        formData: {},
        listData: [],
        isDisabledAction: false,
        category: '',
        error: [],
        tableData: []
    }

    componentDidMount = () => {
        this.handleFetch();
    }

    handleInputOnChange = (event) => {
        const { name, value } = event.target;
        const { formData } = this.state;
        formData[name] = value;

        if (name === 'category') {
            this.setState({ category: value });
        }

        this.setState({ formData });
    };

    handleValidation = () => {
        const { formData } = this.state;
        let errorData = [];

        if (formData.name){
            if(typeof formData["name"] !== "undefined"){
                if(!formData["name"].match(/^[a-zA-Z\s]+$/)){
                    errorData.push({errorField: 'name', errorMessage: 'Name is not valid'});
                }        
             }
        }

        if (formData.email) {
            if (typeof formData["email"] !== "undefined") {
                let lastAtPos = formData["email"].lastIndexOf('@');
                let lastDotPos = formData["email"].lastIndexOf('.');
     
                if (!(lastAtPos < lastDotPos && lastAtPos > 0 && formData["email"].indexOf('@@') === -1 && lastDotPos > 2 && (formData["email"].length - lastDotPos) > 2)) {
                   errorData.push({errorField: 'email', errorMessage: 'Email is not valid'});
                }
            }  
        } 

        if (formData.phone) {
            const phone = /^\+?([0-9]{11})$/;
            if(!formData['phone'].match(phone)) {
                errorData.push({errorField: 'phone', errorMessage: 'Phone number is not valid. Ex: 09272000429'});
            }
        }
        
        this.setState({ error: errorData })
    }

    handleSave = () => {
        const { formData } = this.state;
        http.post(`exam`, formData)
            .then( res => {
                if (res.status === 201) {
                    alert('Successfully added');
                    this.handleFetch();
                }
            })
            .catch(error => {
                const { status } = error.response;
                alert(`Status ${status} - Unable to save, Input required fields`);
            });
    }

    handleFetch = () => {
        http.get(`exam`).then( res => {
            const { data } = res;
            this.setState({ tableData: data });
        }); 
    }

    handleClear = () => {
        this.setState({ formData: {} });
    }

    displayTableData = () => {
        const { tableData } = this.state;
        
        return (
            <TableContainer component={Paper}>
                <Table aria-label="simple table">
                    <TableBody>
                    {tableData.map((row, i) => (
                        <TableRow key={i}>
                            <TableCell>{row.name}</TableCell>
                            <TableCell>{row.email}</TableCell>
                            <TableCell>{row.phone}</TableCell>
                            <TableCell>{row.category}</TableCell>
                            <TableCell>{row.model}</TableCell>
                        </TableRow>
                    ))}
                    </TableBody>
                </Table>
            </TableContainer>
        )
    }

    render() {
        const { formData, isDisabledAction, category, error } = this.state;
        const formFields = generateForm(this.handleInputOnChange, this.handleValidation, category);
        
        return (
            <Fragment>
                <FormEdify
                    fields={formFields}
                    data={formData}
                    errorValidation={error}
                />
                <FormAction 
                    handleSave={this.handleSave}
                    handleClear={this.handleClear}
                    isDisabledAction={isDisabledAction}
                />
                { this.displayTableData() }
            </Fragment>
        );
    }
}

export default Form;
