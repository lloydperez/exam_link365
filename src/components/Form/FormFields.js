export function generateForm(handleInputOnChange, handleValidation, category) {
    let modelOptions = [
        { value: 'none', label: 'None' }
    ];

    if (category === 'small') {
        modelOptions = [
            { value: 'Opel Corsa',       label: 'Opel Corsa'   },
            { value: 'Toyota Yaris',     label: 'Toyota Yaris' },
            { value: 'Samrt for Two',    label: 'Samrt for Two'  },
        ]
    } else if (category === 'premium') {
        modelOptions = [
            { value: 'Audi S8',             label: 'Audi S8'      },
            { value: 'Jaguar XJR',          label: 'Jaguar XJR'   },
            { value: 'bmwJaguar XJR750iL',  label: 'BMW 750iL'    },
        ]
    } else if (category === 'van') {
        modelOptions = [
            { value: 'Volkswagen Touran',  label: 'Volkswagen Touran'   },
            { value: 'Renault Espace',     label: 'Renault Espace'      },
            { value: 'Fiat Talento',       label: 'Fiat Talento'        },
        ]
    } else {
        modelOptions = [
            { value: 'none', label: 'None' }
        ]
    }

    return [
      {
        columns: [
          {
            label: 'Name',
            type: 'textInput',
            dataIndex: 'name',
            onChange: handleInputOnChange,
            variant: 'outlined',
            validate: handleValidation,
          }
        ]
      },
      {
        columns: [
          {
            label: ' E-mail',
            type: 'textInput',
            dataIndex: 'email',
            onChange: handleInputOnChange,
            variant: 'outlined',
            validate: handleValidation
          }
        ]
      },
      {
        columns: [
          {
            label: 'Phone',
            type: 'textInput',
            dataIndex: 'phone',
            onChange: handleInputOnChange,
            variant: 'outlined',
            validate: handleValidation
          }
        ]
      },
      {
        columns: [
          {
            label: 'Car Category',
            type: 'selectInput',
            dataIndex: 'category',
            onChange: handleInputOnChange,
            defaultValue: 'none',
            options: [
                { value: 'none',    label: 'None'    },
                { value: 'small',   label: 'Small'   },
                { value: 'premium', label: 'Premium' },
                { value: 'van',     label: 'Van'     },
            ],
            variant: 'outlined'
          }
        ]
      },
      {
        columns: [
          {
            label: 'Car Model',
            type: 'selectInput',
            dataIndex: 'model',
            onChange: handleInputOnChange,
            nestedOption: true,
            options: modelOptions,
            variant: 'outlined',
            isDisabled: category && category !== 'none' ? '' : 'disabled'
          }
        ]
      },
    ];
  }
  