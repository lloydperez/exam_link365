import React from "react";
// import { ThemeProvider } from '@material-ui/styles';
import Form from "./components/Form/Form";
import "./styles.css";

export default function App() {
  return (
    // <ThemeProvider>
      <div className="App">
        <Form />
      </div>
    // </ThemeProvider>
  );
}
