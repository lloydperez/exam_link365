## Base on material component UI but not a boilerplate

## To run the project
- npm install
- npm start

## Component
- Form (The main component that wrap the form generated)
---- FormField (Object to be added in order to generate the desire fields base on the field type);

## Reusable Components
- FormEdify (To generate form and can be upgrade in the future use)
- FormAction (Action button like save and reset)

## API
- http.js (Create an API instance from axios)

## REST API
- http.get(`exam`) - endpoint to fetch data
- http.get(`exam\${id}`) - endpoint to fetch data by ID
- http.post(`exam`, params) - endpoint to save data
- http.put(`exam\${id}`, params) - endpoint to update data by ID
- http.delete(`exam\${id}`) - endpoint to delete data by ID